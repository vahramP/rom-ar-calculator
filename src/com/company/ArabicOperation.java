package com.company;


public class ArabicOperation {
    void defineArabicOperation(String input) throws WrongInputException {
        Operations operation = new Operations();
        String ops = input.replaceAll("([\\w\\s+])", "");

        switch (ops) {
            case "+":
                String[] splitInput = input.split("\\+");
                int x = Integer.parseInt(splitInput[0].replaceAll("\\s+",""));
                int y = Integer.parseInt(splitInput[1].replaceAll("\\s+",""));

                System.out.println(operation.summary(x, y));
                break;
            case "-":
                splitInput = input.split("\\-");
                x = Integer.parseInt(splitInput[0].replaceAll("\\s+",""));
                y = Integer.parseInt(splitInput[1].replaceAll("\\s+",""));

                System.out.println(operation.subtract(x, y));
                break;
            case "*":
                splitInput = input.split("\\*");
                x = Integer.parseInt(splitInput[0].replaceAll("\\s+",""));
                y = Integer.parseInt(splitInput[1].replaceAll("\\s+",""));

                System.out.println(operation.multiply(x, y));
                break;
            case "/":
                splitInput = input.split("\\/");
                x = Integer.parseInt(splitInput[0].replaceAll("\\s+",""));
                y = Integer.parseInt(splitInput[1].replaceAll("\\s+",""));

                System.out.println(operation.divide(x, y));
                break;
            default:
                throw new WrongInputException("Operation not supported: " + input);
        }
    }
}
