package com.company;

import java.util.Scanner;


public class Input extends Exception {
    void input() throws WrongInputException {
        System.out.print("Enter your operation: ");

        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();

        if (input.equals("Stop")) {
            System.exit(0);
        }
        else {
            extractSigns(input);
        }
    }

    void extractSigns(String input) throws WrongInputException {
        String newInput = null;

        String[] operations = {"+", "-", "*", "/"};

        for (int i = 0; i < operations.length; i++) {
            if (input.contains(operations[i])) {
                newInput = input.replace(operations[i], "").replaceAll("\\s+", "");
                break;
            }
        }
        Validation.validateArabicOrRoman(input, newInput);
    }
}
