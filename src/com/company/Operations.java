package com.company;


public class Operations {
    String summary(int x, int y) throws WrongInputException {
        Validation.validateArabicNumbers(x, y);
        int sum = x + y;

        String result = "Result is " + sum;
        return result;
    }

    String subtract(int x, int y) throws WrongInputException {
        Validation.validateArabicNumbers(x, y);
        int sub = x - y;

        String result = "Result is " + sub;
        return result;
    }

    String multiply(int x, int y) throws WrongInputException {
        Validation.validateArabicNumbers(x, y);
        int mul = x * y;

        String result = "Result is " + mul;
        return result;
    }

    String divide(double x, double y) throws WrongInputException {
        Validation.validateArabicNumbers(x, y);
        double div = x / y;

        String result = "Result is " + div;
        return result;
    }
}
