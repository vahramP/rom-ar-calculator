package com.company;

import java.util.HashMap;
import java.util.Map;


public class RomanOperation {
    void defineRomanOperation(String input) throws WrongInputException {
        Operations operation = new Operations();
        String ops = input.replaceAll("([\\w\\s+])", "");

        switch (ops) {
            case "+":
                String[] splitInput = input.split("\\+");
                String x = splitInput[0].replaceAll("\\s+","");
                String y = splitInput[1].replaceAll("\\s+","");

                System.out.println(operation.summary(findValue(x), findValue(y)));
                break;
            case "-":
                splitInput = input.split("\\-");
                x = splitInput[0].replaceAll("\\s+","");
                y = splitInput[1].replaceAll("\\s+","");

                System.out.println(operation.subtract(findValue(x), findValue(y)));
                break;
            case "*":
                splitInput = input.split("\\*");
                x = splitInput[0].replaceAll("\\s+","");
                y = splitInput[1].replaceAll("\\s+","");

                System.out.println(operation.multiply(findValue(x), findValue(y)));
                break;
            case "/":
                splitInput = input.split("\\/");
                x = splitInput[0].replaceAll("\\s+","");
                y = splitInput[1].replaceAll("\\s+","");

                System.out.println(operation.divide(findValue(x), findValue(y)));
                break;
            default:
                throw new WrongInputException("Operation not supported: " + input);
        }
    }

    int findValue(String romanNumber) throws WrongInputException {
        Validation.validateNumberFormat(romanNumber);

        Map<String, Integer> numbers = new HashMap<String, Integer>();
        numbers.put("I", 1);
        numbers.put("II", 2);
        numbers.put("III", 3);
        numbers.put("IV", 4);
        numbers.put("V", 5);
        numbers.put("VI", 6);
        numbers.put("VII", 7);
        numbers.put("VIII", 8);
        numbers.put("IX", 9);
        numbers.put("X", 10);

        int arabicNumber = 0;

        if(numbers.get(romanNumber) == null) {
            throw new WrongInputException("There is no such Roman number in your application base");
        }
        else {
            arabicNumber = numbers.get(romanNumber);
        }
        return arabicNumber;
    }
}
