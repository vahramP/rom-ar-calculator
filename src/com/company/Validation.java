package com.company;

public class Validation {
    static boolean isNumber(String input) {
        try {
            Integer.parseInt(input);
        }
        catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    static void validateArabicOrRoman(String input, String newInput) throws WrongInputException {

        if (isNumber(newInput)) {
            ArabicOperation arabicOperation = new ArabicOperation();
            arabicOperation.defineArabicOperation(input);
        }
        else if (!isNumber(newInput)) {
            RomanOperation romanOperation = new RomanOperation();
            romanOperation.defineRomanOperation(input);
        }
    }

    static boolean validateNumberFormat(String input) throws WrongInputException {
        try {
            double d = Double.parseDouble(input);
        }
        catch (NumberFormatException nfe) {
            return false;
        }
        throw new WrongInputException("Both numbers must be either whole or Roman");
    }

    static void validateArabicNumbers(double x, double y) throws WrongInputException {
        if(x <= 0 || x > 10 || y <= 0 || y > 10 ) {
            throw new WrongInputException("Wrong input, numbers must be between 1 and 10");
        }
    }
}
